# README #

* Migrating Crontabs from UVMS in Command Line (Instead of from Unijob and GUI)
* Version 0.1
* Author: Brendan SAPIENCE - ORSYP 2012
* bsa@orsyp.com

* WHY?

Because if the Unijob installation is done without root, and the Unijob owner is not root, the conversion of Crons from the GUI will not be possible (except for the owner itself).

If an installation is non-root: all process belong to a non-root user, the unilan, to be able to run jobs as anyone, must have the SUID flag.. but this flag can ONLY be added to unilan. The migration code is located in uniio and it will therefore not work..

this is a workaround :)

