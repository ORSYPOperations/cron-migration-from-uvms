package com.orsyp.specfilters;

import java.util.regex.Pattern;

public class JobSpecFilter  extends SpecFilter{
	
	public String fType="JOB";
	
	public JobSpecFilter(String pattern){
		super(pattern);

		isActive=true;
	}
	
	public String getType(){
		return fType;
	}
	
}
