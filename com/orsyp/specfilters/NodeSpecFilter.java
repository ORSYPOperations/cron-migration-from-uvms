package com.orsyp.specfilters;

import java.util.regex.Pattern;

public class NodeSpecFilter extends SpecFilter{
	
	public NodeSpecFilter(String pattern){
		super(pattern);
		fType="NODE";
	}
	
	public String getType(){
		return fType;
	}

}
