package com.orsyp.specfilters;

import java.util.regex.Pattern;

public class TargetSpecFilter  extends SpecFilter{

	public String fType="TARGET";
	
	public TargetSpecFilter(String pattern){
		super(pattern);
		isActive=true;
	}
	
	public String getType(){
		return fType;
	}
	
}
