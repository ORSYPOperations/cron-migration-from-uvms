package com.orsyp.unijob;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.orsyp.Identity;
import com.orsyp.Environment;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Filter;
import com.orsyp.api.InvalidNumlancException;
import com.orsyp.api.ObjectAlreadyExistException;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.execution.Execution;
import com.orsyp.api.execution.ExecutionFilter;
import com.orsyp.api.execution.ExecutionId;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobStatus;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.api.user.UserType;
import com.orsyp.schedconv.SchedConv;
import com.orsyp.schedconv.SchedImplFactory;
import com.orsyp.schedconv.SchedJob;
import com.orsyp.schedconv.SchedLine;
import com.orsyp.schedconv.SchedTools;
import com.orsyp.schedconv.SchedVariable;
import com.orsyp.schedconv.cron.CronFile;
import com.orsyp.schedconv.cron.CronOsProperties;
import com.orsyp.schedconv.std.SchedStdConnection;
import com.orsyp.schedconv.std.cron.CronStdImpl;
import com.orsyp.schedconv.std.cron.SchedStdCronImplFactory;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.ExecutionListStdImpl;
import com.orsyp.std.ExecutionStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.UserListStdImpl;
import com.orsyp.std.UserStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;


public class UNIJOBInterface {
	
	String group = "*";
	String host = "*";
	String system = "W32";
	Environment env = null;
	Context contextUJ;
	ConnectionFactory factory;
	SchedImplFactory mySchedFact;
	
	public UNIJOBInterface(String login, String node,UniCentral central) throws UniverseException{

		try {
			this.env = new Environment(Product.UNIJOB.toString(), node);
			
		} catch (SyntaxException e) {
			e.printStackTrace();
		}
	Client clientUJ = new Client(new Identity(login, group, host, system));
	this.contextUJ = new Context (env, clientUJ, central);
	contextUJ.setProduct(Product.UNIJOB);
	this.factory = NodeConnectionFactory.getInstance(central);
	ClientConnectionManager.setDefaultFactory(factory);
	//boolean values are: ConnectionFactory, withSecurityManager, errorIsFatal, autoClose, rootIsStdUser
	// optional are logFileName (string) and log level
	this.mySchedFact = new SchedStdCronImplFactory(factory, false, true, true, true);

	}
	
	public void createUser(String userName) throws UniverseException{
		if (!userName.equals("")){
		User myUsr = new User(contextUJ, userName);
		myUsr.setImpl(new UserStdImpl());
		myUsr.setProfile("PROFADM");
		myUsr.setAuthorCode(userName.toUpperCase().substring(0, 3));
		myUsr.setUserType(UserType.Both);
		//myUsr.canCreate();
		try{
			myUsr.create();
			System.out.println("	++ User "+userName+" declared successfully!");
		}
		catch (ObjectAlreadyExistException e){
			System.err.println("	-- Warning: User "+userName+" already exists.. ignoring.");
			}
		}
	}
	
	public String launchJob(String jobname,String target,String user) throws UniverseException, InterruptedException{
		
		JobRun myRun = new JobRun(jobname,target,user,this.contextUJ);
		//myRun.displayDates();
		String NumLanc=myRun.launchJob();
		
		ExecutionFilter execFilter = new ExecutionFilter();
		execFilter.setUprocName(jobname);
		execFilter.setNumlancMin(NumLanc);
		execFilter.setNumlancMax(NumLanc);
		String StatusCheck=null;
		// necessary sleep, because the status is initiated with A in the product
		Thread.sleep(500);
		Execution exec=null;
		while(StatusCheck==null || ((!StatusCheck.equals("T")) && (!StatusCheck.equals("A")))){
		
		ExecutionList myList = new ExecutionList(contextUJ,execFilter);
		myList.setImpl(new ExecutionListStdImpl());
		myList.extract();
			

		
			ExecutionItem it = myList.get(0);
			exec = new Execution(it);
			exec.setImpl(new ExecutionStdImpl());
			exec.extract();

			StatusCheck=exec.getStatus().toString();
			//System.out.println("DEBUG:"+exec.getNumlanc()+":"+exec.getStatus().toString());
		}
		if(StatusCheck.equals("A")){
			System.err.println("ERROR!! -> Job "+jobname+" Aborted !");
			System.exit(1);
		}
		

		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatProcDate = new SimpleDateFormat("yyyyMMddhhmmss");
        String FileName="c:/tmp/"+jobname+"_Tmp_"+dateFormatProcDate.format(calendar.getTime());
		File LogFile = new File(FileName);
		exec.getExecutionLog(LogFile);
		return FileName;
	}
	
	public List<SchedLine> getCronsAsList(String sched, String user, String inputFile) throws UniverseException, IOException{

		SchedConv mySchedConv = new SchedConv(contextUJ, mySchedFact);

		// params are context, factory and autoClose
		SchedStdConnection myConn = new SchedStdConnection(contextUJ, factory, true);
		
		// params are factory, connectAutoClose, rootIsStdUser and errorIsFatal
		CronStdImpl myCronIm = new CronStdImpl(factory, true, true, true);
		CronOsProperties myOS = new CronOsProperties(SchedTools.getOsName(contextUJ));
		CronFile myFile = new CronFile(contextUJ, mySchedFact, SchedTools.getOsName(contextUJ), sched, user, inputFile);

		FileReader fileReader = new FileReader(myFile.getFilePath());
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> cronArray = new ArrayList<String>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
        	cronArray.add(line);
        }
        bufferedReader.close();

		myFile.setLinesArray(cronArray.toArray(new String[]{}));
		myFile.getSchedLineList();
		myFile.parseLines(myOS, false, mySchedFact.getCronImpl());
		//List<SchedLine> allSchedLines = myFile.getSchedLineList();
		return myFile.getSchedLineList();
	}
	
	public SchedJob buildJobFromLine(SchedLine schedLine) throws UniverseException, IOException{
		SchedJob mySchedJob = new SchedJob(contextUJ,mySchedFact,schedLine,null);
		ArrayList<SchedVariable> schedVarList = new ArrayList();
		SchedVariable myHome = new SchedVariable("HOME", "/home/bsa/", "Text");
		SchedVariable myShell = new SchedVariable("SHELL", "/usr/ksh/", "Text");

		schedVarList.add(myHome);
		schedVarList.add(myShell);
		mySchedJob.setSchedVarList(schedVarList);

		mySchedJob.buildUniObj();
		
		//mySchedJob.print(mySchedJob.getJob());
		Job theJob = mySchedJob.getJob();
		theJob.setComment("ta mere");
		theJob.setStatus(JobStatus.DISABLED);
		mySchedJob.setJob(theJob);
		
		return mySchedJob;
		

	}
	
}
