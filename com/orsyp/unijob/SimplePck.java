package com.orsyp.unijob;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.orsyp.UniverseException;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.std.deploymentpackage.PackageStdImpl;

public class SimplePck {
	
	public SimplePck(UVMSInterface uvInterface,String NodeToProcess) throws UniverseException, IOException{

	Package importPck = new Package(uvInterface.getContext(),new PackageId("UNIJOB_CRON_MIG_TECH_PACK"));
	importPck.setImpl(new PackageStdImpl());
	
	InputStream in = getClass().getResourceAsStream("/com/orsyp/UJ_CRONMIGKIT.unipkg");
	
	File f=new File("c:/tmp/tmp.pckage");
	  OutputStream out=new FileOutputStream(f);
	  byte buf[]=new byte[1024];
	  int len;
	  while((len=in.read(buf))>0)
	  out.write(buf,0,len);
	 out.close();
     in.close();
	//File impFile=new File("c:/tmp/UJ_CRONMIGKIT.unipkg");
	importPck.importPackage(f);
	importPck.save();
	importPck.deploy(NodeToProcess, true);
	if(!importPck.getOperationStatus().toString().equalsIgnoreCase("OK")){
		System.err.println("Error deploying the tech package...");
		System.err.println(importPck.getOperationStatus());
		System.exit(1);
	}
	}
}
