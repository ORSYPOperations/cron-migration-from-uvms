package com.orsyp.unijob;

import com.orsyp.OutcomeReport;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobId;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskItem;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskSpecificData;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.protocols.JobProtocol.LaunchHourPattern;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.MuListStdImpl;
import com.orsyp.std.MuStdImpl;
import com.orsyp.std.TaskListStdImpl;
import com.orsyp.std.TaskStdImpl;
import com.orsyp.std.UserListStdImpl;
import com.orsyp.std.UserStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;
import com.orsyp.std.nfile.LocalBinaryFile;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class UJJobPackage {

	private String nodeName;
	//private int uJPort;
	private String packageName;
	private Context context;
	private Context UJcontext;	
	private String pckSavePath;
	private Boolean ExportPck;
	public OutcomeReport rep;
	public SpecFilter[] allFilters;
	public boolean verbose;

	public UJJobPackage(String nodeName, String packageName, int uJPort, Context context, Context UJcontext, 
			String pckSavePath, Boolean ExportPck, OutcomeReport rep, SpecFilter[] allFilters, boolean verbose) throws UniverseException {
		this.nodeName=nodeName;
		//this.uJPort = uJPort;
		this.context = context;
		this.UJcontext = UJcontext;
		this.packageName = packageName;
		this.pckSavePath = pckSavePath;
		this.ExportPck = ExportPck;
		this.rep = rep;
		this.allFilters=allFilters;
		this.verbose=verbose;
	}
	
	public void extractPackage(boolean Jobs, boolean Users, boolean Targets, boolean Calendars) throws UniverseException{
			
		int jlcount=0;
		int mlcount=0;
		int ulcount=0;
		//int clcount=0;
			//ServerConnection sco = new ServerConnection(nodeName,uJPort);
		
		/**
		 * Setting Up Package for Object insertion
		 **/
		
			packageName = setPckName(packageName);
			String ExpPackageName = setExpPckName(packageName);
			PackageId pID = new PackageId(packageName);
			Package savePck = new Package(context,pID);
			savePck.setComment("Automatically Generated from Node: "+nodeName);
			savePck.setImpl(new PackageStdImpl());
			
			/**
			 * Extracting Lists of Objects (Targets, Users & Jobs
			 **/
			
			com.orsyp.api.job.JobList jl = new com.orsyp.api.job.JobList(UJcontext);
			jl.setImpl(new JobListStdImpl());
			com.orsyp.api.user.UserList ul = new com.orsyp.api.user.UserList(UJcontext);
			ul.setImpl(new UserListStdImpl());
			com.orsyp.api.mu.MuList ml = new com.orsyp.api.mu.MuList(UJcontext);
			ml.setImpl(new MuListStdImpl());
			//com.orsyp.api.calendar.CalendarList cl = new com.orsyp.api.calendar.CalendarList(UJcontext);
			//cl.setImpl(new CalendarListStdImpl());
		     JobSpecFilter jfilter = (JobSpecFilter) allFilters[1];
		     TargetSpecFilter tfilter = (TargetSpecFilter) allFilters[2];
		     UserSpecFilter ufilter = (UserSpecFilter) allFilters[3];

				try {
					jl.extract();
					ul.extract();
					ml.extract();
					//cl.extract();
					
				} catch (UniverseException u){
				// to do	
				}
				/**
				 * Each type of object follows the same logic:
				 * an object item must be created (from an element of the  retrieved)
				 * an Object ID is created from the Item
				 * the Object itself is copied in an instance of object (context + ID)
				 * the std implementation is put in place
				 * only then => the extraction
				 **/
				
			if (Jobs){
				jlcount=jl.getCount();
				for (int j = 0; j < jl.getCount(); j++) {		
					PackageElement pck_el = new PackageElement();
					JobItem it = jl.get(j);	
					JobId ID = it.getIdentifier();
					Job Ujob = new Job(UJcontext,ID);
					Ujob.setImpl(new JobStdImpl());
					Ujob.extract();
					if(jfilter.matchespattern(Ujob.getName())){
					rep.addnJobExt(1);
					Ujob.setTargetName("XXX_TARGET_DEFAULT");				
					pck_el.setJob(Ujob);		
					savePck.addOrUpdatePackageElement(pck_el);
					}else if(!jfilter.matchespattern(Ujob.getName()) && verbose){
						System.out.println("   %%% Job "+Ujob.getName()+" skipped [filtered].");
					}
				}
			}
			if (Users){
				ulcount=ul.getCount();
				for (int j = 0; j < ul.getCount(); j++) {		
					PackageElement pck_el = new PackageElement();
					UserItem it = ul.get(j);	
					UserId ID = it.getIdentifier();
					User MyUser = new User(UJcontext,ID);
					MyUser.setImpl(new UserStdImpl());
					MyUser.extract();
					//rep.addnJobExt(1);	
					if(ufilter.matchespattern(MyUser.getName())){
					pck_el.setUser(MyUser);		
					savePck.addOrUpdatePackageElement(pck_el);
					} else if(!jfilter.matchespattern(MyUser.getName()) && verbose){
						System.out.println("   %%% user "+MyUser.getName()+" skipped [filtered].");
					}
				}
			}
			if (Targets){	
				mlcount=ml.getCount();
				for (int j = 0; j < ml.getCount(); j++) {		
					PackageElement pck_el = new PackageElement();
					MuItem it = ml.get(j);	
					MuId ID = it.getIdentifier();
					Mu MyMu = new Mu(UJcontext,ID);
					MyMu.setImpl(new MuStdImpl());
					MyMu.extract();
					//rep.addnJobExt(1);	
					if(tfilter.matchespattern(MyMu.getName())){
					pck_el.setMu(MyMu);		
					savePck.addOrUpdatePackageElement(pck_el);
					}else if(!tfilter.matchespattern(MyMu.getName()) && verbose){
						System.out.println("   %%% Target "+MyMu.getName()+" skipped [filtered].");
					}
				}
			}
			savePck.save();
			rep.addnPackageExt(1);

			System.out.println("    +++ Package: [ Name | nb of Jobs | nb of Users | nb of Targets ]: [ " 
					+packageName+" | "+jlcount+" | "+ulcount+" | "+mlcount+" ]" );	
			if (ExportPck){				
				LocalBinaryFile myFile = new LocalBinaryFile(ExpPackageName);
				savePck.exportPackage(myFile);
				System.out.println("    +++ Package Exported: " + ExpPackageName);
				}
			 System.out.println("");
	}
	
	public String setPckName(String name){
		if (name.equals("")){
			
	DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HHmmss");
    Date date = new Date();
    packageName=nodeName+"_"+dateFormat.format(date);
		} else{
			packageName=name;
		}
		return packageName;
	}
	
	public String setExpPckName(String name){
		
		return pckSavePath+name+".unipkg";
	}

}
