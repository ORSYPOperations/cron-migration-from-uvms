package com.orsyp.unijob;

import java.util.Calendar;
import java.util.Date;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.PackageSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.deploymentpackage.PackageListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;

public class UJPackageClear {

	private int DayShift;
	private int HourShift;
	private int MinuteShift;
	private Context context;
	//private String pckSavePath;
	private boolean purgeActive;
	public SpecFilter[] allFilters;
	public boolean verbose;

	
	public UJPackageClear(boolean purgeActive, int DayShift, int HourShift, int MinuteShift, Context context 
			,String pckSavePath, SpecFilter[] allFilters, boolean verbose){
		
		this.DayShift=DayShift;
		this.HourShift=HourShift;
		this.MinuteShift=MinuteShift;
		this.context = context;
		//this.pckSavePath = pckSavePath;
		this.purgeActive=purgeActive;
		this.allFilters=allFilters;
		this.verbose=verbose;
	} 
	
	public void cleanupPackage() throws UniverseException{
		
		PackageList pckList = new PackageList(context);
		pckList.setImpl(new PackageListStdImpl());
		pckList.extract();
		Date d2 = getShiftDate(DayShift,HourShift,MinuteShift);
		System.out.println("    +++ Cleaning packages older than: "+d2);
		
		for (int i = 0; i < pckList.getCount();i++){
			PackageItem pck = pckList.get(i);
			Date d1 = pck.getLastModificationDate();
			if (d1.before(d2) && purgeActive){

			PackageId pckID = pck.getIdentifier();
			com.orsyp.api.deploymentpackage.Package myPck = new com.orsyp.api.deploymentpackage.Package(context, pckID);
			myPck.setImpl(new PackageStdImpl());
			myPck.extract();
			
		     PackageSpecFilter pfilter = (PackageSpecFilter) allFilters[4];
		 	if(pfilter.matchespattern(myPck.getName())){
			//myPck.get
			System.out.println("       Deleting Package: "+myPck.getName());
			myPck.delete();
		 	}else if(!pfilter.matchespattern(myPck.getName()) && verbose){
		 		System.out.println("   %%% Package "+myPck.getName()+" skipped [filtered].");
		 	}
			}
		}
	}
	
	public Date getShiftDate(int Day,int Hour,int Minute){
		Date CurrentDate = new Date();
		Calendar c=Calendar.getInstance();
		c.setTime(CurrentDate);
		c.add(Calendar.DAY_OF_YEAR,-Day);
		c.add(Calendar.HOUR, -Hour);
		c.add(Calendar.MINUTE, -Minute);
		Date d2=c.getTime();
		return d2;
	}
}
