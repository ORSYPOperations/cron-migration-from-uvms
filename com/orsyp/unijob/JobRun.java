package com.orsyp.unijob;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.orsyp.Syntax;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.InvalidNumlancException;
import com.orsyp.api.launch.Launch;
import com.orsyp.api.launch.LaunchId;
import com.orsyp.api.launch.LaunchItem;
import com.orsyp.api.launch.LaunchList;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.LaunchListStdImpl;
import com.orsyp.std.LaunchStdImpl;
import com.orsyp.util.DateTools;

public class JobRun {

	Context ctx;
	String JobName;
	String MUName;
	String SubUser;
	String BeginDateDayStr;
	String BeginDateTimeStr;
	String EndDateDayStr;
	String EndDateTimeStr;
	
	private static String DefQueue="SYS_BATCH";
	private static String DefPrinter="IMPR";
	private static String DefPriority="1";
	private String ProcDate;
	
	
	public JobRun(String job, String mu, String user,Context ctx){
		this.ctx=ctx;
		this.JobName=job;
		this.MUName=mu;
		this.SubUser=user;
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatProcDate = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateFormatLaunchDate = new SimpleDateFormat("hhmmss");

        this.ProcDate=dateFormatProcDate.format(calendar.getTime());
		calendar.add(Calendar.HOUR, -1);
		this.BeginDateDayStr=dateFormatProcDate.format(calendar.getTime());
		this.BeginDateTimeStr=dateFormatLaunchDate.format(calendar.getTime());
		calendar.add(Calendar.HOUR, +8);

        this.EndDateDayStr=dateFormatProcDate.format(calendar.getTime());
        this.EndDateTimeStr=dateFormatLaunchDate.format(calendar.getTime());
		
	}
	
	public void displayDates(){
		
		System.out.println("Proc Date:"+this.ProcDate);
		System.out.println("Begin Date Day:"+this.BeginDateDayStr);
		System.out.println("Begin Date Time:"+this.BeginDateTimeStr);
		System.out.println("End Date Day:"+this.EndDateDayStr);
		System.out.println("End Date Time:"+this.EndDateTimeStr);
		
	}
	
	public String launchJob() throws UniverseException{
		
		String jobVersion="000";
		String numlanc="0001111";
		LaunchId myId = LaunchId.createWithName(null,null,JobName,jobVersion,this.MUName,numlanc);
		 Launch myLaunch = new Launch(ctx, myId);
		 myLaunch.setImpl(new LaunchStdImpl());
		 myLaunch.setUprocName(JobName);
		 myLaunch.setProcessingDate("20120220");
		 myLaunch.setUserName(this.SubUser);
		 myLaunch.setQueue(this.DefQueue);
		 myLaunch.setPriority(this.DefPriority);
		 myLaunch.setPrinter(this.DefPrinter);
		 
		// System.out.println("It is:"+DateTools.toDate(this.BeginDateDayStr,this.BeginDateTimeStr));
		// System.out.println("It is:"+DateTools.toDate(this.EndDateDayStr,this.EndDateTimeStr));
		 
		 myLaunch.setBeginDate(DateTools.toDate(this.BeginDateDayStr,this.BeginDateTimeStr));
		 myLaunch.setEndDate(DateTools.toDate(this.EndDateDayStr,this.EndDateTimeStr));
		 myLaunch.create();
/*
		 LaunchList myList = new LaunchList(ctx);
		 myList.setImpl(new LaunchListStdImpl());
		 myList.extract();
		 Date date1=null;
		 Date date2=null;
			for (int j = 0; j < myList.getCount(); j++) {	
				LaunchItem it = myList.get(j);
				LaunchId id = it.getIdentifier();
				Launch tLaunch = new Launch(ctx,id);
				tLaunch.setImpl(new LaunchStdImpl());
				tLaunch.extract();
				date1=tLaunch.getBeginDate();
				date2=tLaunch.getEndDate();
				
			}
		 */
			 return myLaunch.getNumlanc();
	}
	
}
