package com.orsyp.util;

public class MathUtil {

	public static String IncrementAsString(String val, int numberOfChar)
	{
		try{
			if (val==null)val="0";
			String result=""+(Integer.parseInt(val)+1);
			while (result.length() < numberOfChar )
				result="0"+result;
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
}
