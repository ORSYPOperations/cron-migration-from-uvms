package com.orsyp.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import com.orsyp.reppub.common.exception.FileNotFoundException;

public class CronParser {
	
	private File CRONFile;
	public ArrayList<String> ListOfUsers = new ArrayList();

	public CronParser(String cronFilePath) throws FileNotFoundException, java.io.FileNotFoundException {
		this.CRONFile = new File(cronFilePath);
		getListOfUsers();
		
	}

	public void getListOfUsers() throws java.io.FileNotFoundException, FileNotFoundException {
        Scanner scanner = new Scanner(this.CRONFile);
 
		while (scanner.hasNextLine()) {
		    String line = scanner.nextLine();
		    if(line.startsWith("++")){
		    	String[] items = line.split("\\^");
		    	if(!items[3].isEmpty() && (!items[2].contains("user")&&!items[2].contains("unknown"))){
		    	if(this.ListOfUsers==null || !this.ListOfUsers.contains(items[1])){
		    		this.ListOfUsers.add(items[1]);	
		    	}	    	
		    	}
		    }
		}
		scanner.close();

	}

	public String buildCronFileforUser(String usr) throws IOException {
	       Scanner scanner = new Scanner(this.CRONFile);
	       Calendar calendar = Calendar.getInstance();
	       SimpleDateFormat dateFormatProcDate = new SimpleDateFormat("yyyyMMddhhmmss");
	       String FileName="c:/tmp/"+usr+"_Cron_"+dateFormatProcDate.format(calendar.getTime());
	       FileWriter fstream = new FileWriter(FileName);
	       BufferedWriter out = new BufferedWriter(fstream);
	       //Close the output stream

			while (scanner.hasNextLine()) {
			    String line = scanner.nextLine();
			    if(line.startsWith("++")){
			    	String[] items = line.split("\\^");
			    	if(items[1].equals(usr)){
			    		String pattern=items[2];
			    		String cmd=items[3];
			    		out.write(pattern+ " "+cmd);
			    		out.newLine();
			    	}	    	
			    }
			}
		       out.close();
			scanner.close();

		
		
		return FileName;
	}

}
