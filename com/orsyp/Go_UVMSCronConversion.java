package com.orsyp;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import org.jfree.util.Log;
import com.orsyp.parser.CronParser;
import com.orsyp.schedconv.SchedConv;
import com.orsyp.schedconv.SchedImplFactory;
import com.orsyp.schedconv.SchedJob;
import com.orsyp.schedconv.SchedJobSchedule;
import com.orsyp.schedconv.SchedLine;
import com.orsyp.schedconv.SchedTools;
import com.orsyp.schedconv.SchedVariable;
import com.orsyp.schedconv.cron.CronFile;
import com.orsyp.schedconv.cron.CronImpl;
import com.orsyp.schedconv.cron.CronOsProperties;
import com.orsyp.schedconv.cron.SchedConvCronImpl;
import com.orsyp.schedconv.std.SchedStdConnection;
import com.orsyp.schedconv.std.cron.CronStdImpl;
import com.orsyp.schedconv.std.cron.SchedStdCronImplFactory;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.NodeSpecFilter;
import com.orsyp.specfilters.PackageSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.StdImplFactory;
import com.orsyp.std.central.network.NetworkNodeListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;
import com.orsyp.std.security.StdSecurityManager;
import com.orsyp.unijob.SimplePck;
import com.orsyp.unijob.UJJobPackage;
import com.orsyp.unijob.UJPackageClear;
import com.orsyp.unijob.UNIJOBInterface;
import com.orsyp.unijob.UVMSInterface;
import com.orsyp.util.PropertyLoader;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.networknode.NetworkNodeItem;
import com.orsyp.api.central.networknode.NetworkNodeList;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobId;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.job.JobStatus;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskSpecificData;
import com.orsyp.api.user.User;
import com.orsyp.help.*;
import edu.mscd.cs.jclo.JCLO;

class AllArgs {

    private boolean help;
    private String pwd;
    private String login;
    private int port;
    private String uvms;
    private String pckname;
    private boolean version;
    private String purge;
    private String job;
    private String node;
    private String user;
    private String target;
    private boolean verbose;
    private boolean sim;
    private String pck;
}

public class Go_UVMSCronConversion {
	
	static String Customer = "CLSA";
	static String Release="0.1";
	static String[] APIversions = {"300"};
	
	public static void main(String[] argv) throws IOException, InterruptedException {
			
		Boolean ExportPck=false;
		String type="UNIJOB";
		
		// UJ port is retrieved from UVMS once connected
		int UJPort;
		String password="";
		String login="";
		String UVMSHost="";
		int port=0;
		String packageName="";
		String pckSavePath="";
		boolean displayHelp=false;
		boolean displayVer=false;
		String RetentionDelay;
		String nodeFilter="";
		String userFilter="";
		String targetFilter="";
		String jobFilter="";
		String packageFilter="";
		boolean verbose=false;
		boolean sim=false;
		
		try {
				System.out.println("=======================================================");
				System.out.println("  **   ORSYP Cron Conversion Tool  version " + Release +"      **  ");
				System.out.println("  *          ORSYP Professional Services.           * ");
				System.out.println("  * Copyright (c) 2012 ORSYP.  All rights reserved. * ");
				System.out.println("  **             Implemented for "+Customer+"              **");
				System.out.println("=======================================================");
				System.out.println("");
				
		        JCLO jclo = new JCLO (new AllArgs());
		        jclo.parse (argv);
		        
		        displayHelp=jclo.getBoolean ("help");
		        displayVer=jclo.getBoolean ("version");
		        password=jclo.getString ("pwd");
		        login=jclo.getString ("login");
		        UVMSHost=jclo.getString ("uvms");
		        port=jclo.getInt ("port");
		        packageName=jclo.getString ("pckname");
		        RetentionDelay=jclo.getString ("purge");
		        jobFilter=jclo.getString ("job");
		        targetFilter=jclo.getString ("target");
		        nodeFilter=jclo.getString ("node");
		        userFilter=jclo.getString ("user");
		        packageFilter=jclo.getString ("pck");
		        verbose=jclo.getBoolean ("verbose");
		        sim=jclo.getBoolean ("sim");
		        
		        if(displayHelp){OnlineHelp.displayHelp(0,"Display help",Release);}
		        if(displayVer){OnlineHelp.displayVersion(0,Release,APIversions);}

			    
		        Properties pGlobalVariables = PropertyLoader.loadProperties("CronConverter");
			    if (password==null || password.equals("")){password=pGlobalVariables.getProperty("UVMS_PWD");}    
			    if (password==null || password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}
			         
				System.out.println("=> Loading Program Options...");

	            if (login == null || login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
	            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
	            if (port==0){port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
  
				UVMSInterface uvInterface = new UVMSInterface(UVMSHost,port,login,password,pGlobalVariables.getProperty("DU_NODE"),pGlobalVariables.getProperty("DU_COMPANY"),Area.Exploitation);
				
				UniCentral central = uvInterface.ConnectEnvironment();
				UJPort = Integer.parseInt(central.getCentralConfigurationVariable("DEF_UJ_IO_PORT"));
				
				List<String> ConnectedUNIJOBList = uvInterface.getConnectedUNIJOB();
				
				Iterator iNodes = ConnectedUNIJOBList.iterator();
				// 0 - Pick a Node
				System.out.println("=> List of Connected Nodes:");
				 while(iNodes.hasNext()){
					 System.out.println("	+ "+iNodes.next());
				 }		
				 Console c = System.console();
				 String node=null;
				 if (c != null){
				 node = c.readLine("\nPlease Enter The Agent Name to Process From List Above [Only 1]: ");
				 }
				 
				// 1 - Connect a Node
				 int idx=0;
				//CONSOLE EMUL //String NodeToProcess = ConnectedUNIJOBList.get(idx);
				String NodeToProcess = node; //"CLSA_TEST02_bsalinux06"; //node;
				System.out.println("\n=> Connecting to Node" + NodeToProcess);
				UNIJOBInterface UniJob = new UNIJOBInterface(login,NodeToProcess, central);
				System.out.println("	+ Done!");
				// 2 - Import necessary technical Jobs
				System.out.println("\n=> Importing Technical Package containing internal Jobs..");
				SimplePck techPck = new SimplePck(uvInterface,NodeToProcess);
				System.out.println("	+ Done!");
				// 4 - Retrieve the list of System Users with technical Job (TODO)
				System.out.println("\n=> Retrieving the list of System Users from the agent:");
				String UserFilePath=UniJob.launchJob("UJ_CRONMIGKIT_USERLIST_000",NodeToProcess.toUpperCase(),"root");
			     BufferedReader br = new BufferedReader(new FileReader(UserFilePath));
			     String line = null;
			      while ((line = br.readLine()) != null) {
			        
			        if ((!line.trim().contains("u_batch") && !line.trim().contains("COMMAND") && !line.trim().contains("COMMAND-RETURN-CODE")) && !line.trim().isEmpty()) {
			        	System.out.println("	+ "+line);
			        }
			      }
			      br.close();
				//System.out.println("	+ Done!");
				//System.exit(0);
				// 5 - Ask which Users to create (TODO)
			      String userListStr="";
					 if (c != null){
						 userListStr = c.readLine("\n Please Enter The Names of the Users you want to declare [separeted by commas]: ");
						 }

				// 6 - Create in Unijob all necessary Users
				String[] listOfUserstoCreate = userListStr.split("\\,");
				if(listOfUserstoCreate.length>0){
					
				for (int u=0;u<listOfUserstoCreate.length;u++){
				UniJob.createUser(listOfUserstoCreate[u]);

				}
				}
				System.out.println("	+ Done!");
				// 7 - Retrieve the list of Crons for users with technical Job
				System.out.println("\n=> Retrieving the list of Crons for declared Users..");
				String CronFilePath=UniJob.launchJob("UJ_CRONMIGKIT_CRONLIST_001",NodeToProcess.toUpperCase(),"root");
				System.out.println("	+ Done!");
				// 8 - Parse the master Cron File
				System.out.println("\n=> Parsing Cron Master Definition File..");
				CronParser parser = new CronParser(CronFilePath);
				System.out.println("	+ Done!");
				// 9 - Retrieve the total list of users which Crons will be created
				System.out.println("\n=> Parsing Crons for each user..");
				Iterator users = parser.ListOfUsers.iterator(); 
				String userCronPath=null;
				
				// 10 - For each User: build a single Cron File, parse it and convert it to Jobs + copy in Packages
				while(users.hasNext()) {

					String myUser=(String) users.next();						
					System.out.println("\n	=> PROCESSING:"+myUser);
					System.out.println("		=> Creating Job Definitions for User: "+myUser);
					userCronPath = parser.buildCronFileforUser(myUser);
					
				List<SchedJob> allSchedJobs = new ArrayList<SchedJob>();
				//List<SchedLine> allSchedLines = UniJob.getCronsAsList("cron", "att", "c:/Temp/CronTest.txt");
				List<SchedLine> allSchedLines = UniJob.getCronsAsList("cron",myUser,userCronPath);
					Iterator CronList = allSchedLines.iterator();
					
					 while(CronList.hasNext()){
						 allSchedJobs.add(UniJob.buildJobFromLine((SchedLine) CronList.next()));
					}
					 
					 // Job Creation or In Package ?

					 //String packageName = "myPackage";
						System.out.println("		=> Creating Package: "+myUser);
					 PackageId pID = new PackageId("USER["+myUser+"] NODE["+NodeToProcess+"]_ConvertedJobPackage");
					 Package savePck = new Package(uvInterface.getContext(),pID);
					 savePck.setImpl(new PackageStdImpl());
					 savePck.setComment("Automatically Generated During Cron Conversion Process");

					 Iterator SchedJobList = allSchedJobs.iterator();
					 while(SchedJobList.hasNext()){
						 
						 //uncomment below line if we want the job created in Unijob
						 //((SchedJob) SchedJobList.next()).createUniObj();
						 
						Job myJob = ((SchedJob) SchedJobList.next()).getJob();
						myJob.setTargetName("XXX_TARGET_DEFAULT");		
						PackageElement pck_el = new PackageElement();
						pck_el.setJob(myJob);		
						savePck.addOrUpdatePackageElement(pck_el);
					
					 }
						System.out.println("		=> Saving Package for Node: "+myUser);
						savePck.save();
				}
				System.out.println("\n+++ End of Processing.");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
	}
			
public static boolean convertOption(String s){
	if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
		return true;
	}
	return false;
}
}